<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Amp\Http\Client\HttpClientBuilder;
use Amp\Http\Client\HttpException;
use Amp\Http\Client\Request;
use Amp\Http\Client\Response;
use Amp\Loop;
use Throwable;
use function Amp\call;

class GetCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course:get {from} {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get cross course';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Throwable
     */
    public function handle()
    {
        $from = $this->argument('from');
        $to = $this->argument('to');

        Loop::run(static function () use ($from, $to) {
            $currencies = [
                'from' => $from,
                'to' => $to,
            ];

            // Instantiate the HTTP client
            $client = HttpClientBuilder::buildDefault();

            $requestHandler = static function (string $currency) use ($client) {
                /** @var Response $response */
                $request = new Request('http://www.nbrb.by/api/exrates/rates/' . $currency . '?parammode=2');
                $response = yield $client->request($request);
                return yield $response->getBody()->buffer();
            };

            try {
                $result = [];
                $promises = [];
                foreach ($currencies as $key => $currency) {
                    if ($currency === 'BYN') {
                        $result[$key] = 1;
                    } else {
                        $promises[$key] = call($requestHandler, $currency);
                    }
                }
                $bodies = yield $promises;
                foreach ($bodies as $key => $body) {
                    $data = json_decode($body, true);
                    if ($data === null || !isset($data['Cur_OfficialRate'])) {
                        throw new \Exception('Invalid response for currency: ' . $currencies[$key]);
                    }
                    $result[$key] = $data['Cur_OfficialRate'];
                }

                echo "\n====\n" . round($result['to'] / $result['from'], 2) . "\n====\n";

            } catch (HttpException $error) {
                echo $error;
            }
        });
    }
}
